set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '

####################
#                  #
#   ca01.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca01.tex"
#####################
y0	= 0.6174  
xc	= 4049.34457
w	= 0.43974 
A	= 0.88584 
set label 1 'Calibration 01, $\lambda = 4046.6\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4044.8
xh = 4053.2
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca01.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca01.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca02.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca02.tex"
#####################
y0	= 0.66161 	
xc	= 4080.83437
w	= 0.52441 	
A	= 0.87247 	
set label 1 'Calibration 02, $\lambda = 4077.8\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4075
xh = 4085.2
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca02.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca02.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca03.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca03.tex"
#####################
y0	= 0.61287 	
xc	= 4361.32925	
w	= 0.47429 	
A	= 0.44906 	
set label 1 'Calibration 03, $\lambda = 4358.4\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4356.0 
xh = 4365.2 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca03.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca03.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca04.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca04.tex"
#####################
y0	= 0.77697 
xc	= 4415.77582	
w	= 0.44317 
A	= 0.23839 
set label 1 'Calibration 04, $\lambda = 4414.6\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4411.8 
xh = 4418.2
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca04.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca04.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca05.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca05.tex"
#####################
y0	= 0.62526 
xc	= 4681.01411	
w	= 0.35448 
A	= 0.63622 
set label 1 'Calibration 05, $\lambda = 4678.2\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4675.6
xh = 4685.1 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca05.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca05.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca06.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca06.tex"
#####################
y0	= 0.6132  	
xc	= 4802.96565		
w	= 0.57356 	
A	= 0.47961 	
set label 1 'Calibration 06, $\lambda = 4799.9\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4797.2 	  
xh = 4805.7 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca06.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca06.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca07.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca07.tex"
#####################
y0	= 0.74584 
xc	= 4919.44123		
w	= 0.63243 
A	= 0.73768 
set label 1 'Calibration 07, $\lambda = 4916.0\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 4915.4  	  
xh = 4922.5 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca07.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca07.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca08.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca08.tex"
#####################
y0	= 0.61239 	
xc	= 5088.96355	
w	= 0.62776 	
A	= 0.49441 	
set label 1 'Calibration 08, $\lambda = 5085.5\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 5082.4 	  
xh = 5092.6 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca08.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca08.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   ca09.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca09.tex"
#####################
y0	= 0.61796 
xc	= 5463.54616
w	= 0.61127 
A	= 0.94751 
set label 1 'Calibration 09, $\lambda = 5460.7\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 5458.4 	  
xh = 5466.4 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca09.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca09.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   ca10.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca10.tex"
#####################
y0	= 0.6354  	
xc	= 5772.78548
w	= 0.85649 	
A	= 1.18035 	
set label 1 'Calibration 10, $\lambda = 5769.6\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 5766.8  	  
xh = 5776.9
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca10.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca10.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################



####################
#                  #
#   ca11.dat       #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca11.tex"
#####################
y0	= 0.63622 	
xc	= 5793.65843
w	= 0.75787 	
A	= 0.89613 	
set label 1 'Calibration 11, $\lambda = 5790.7\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 5788.6  	  
xh = 5797.6 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca11.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca11.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   ca12-1.dat     #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca12-1.tex"
#####################
y0	= 0.63592 	
xc	= 6442.20217
w	= 0.59812 	
A	= 1.01283 	
set label 1 'Calibration 12-1, $\lambda = 6438.5\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 6435.4  	  
xh = 6445.5
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca12-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca12-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   ca12-2.dat     #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca12-2.tex"
#####################
y0	= 0.63711 
xc	= 6442.10139
w	= 0.65467 
A	= 1.08087 
set label 1 'Calibration 12-2, $\lambda = 6438.5\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 6437.0  	  
xh = 6445.7 
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca12-2.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca12-2.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   ca12-3.dat     #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca12-3.tex"
#####################
y0	= 0.64337 	
xc	= 6442.16807
w	= 0.6233  	
A	= 1.05681 	
set label 1 'Calibration 12-3, $\lambda = 6438.5\ \mathrm{\AA}$' at graph 0,1.03
#####################
xl = 6437.4   	  
xh = 6444.8  
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'ca12-3.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'ca12-3.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################




####################
#                  #
#   slit10-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit10.tex"
#####################
y0	= 0.64493   
xc	= 5772.93738
w	= 0.81504 
A	= 1.18393 
set label 1 'Slit width $=\ 10\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit10-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit10-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   slit12-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit12.tex"
#####################
y0	= 0.62082 	
xc	= 5772.91024
w	= 0.80233 	
A	= 0.48832 	
set label 1 'Slit width $=\ 12\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit12-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit12-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit14-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit14-1.tex"
#####################
y0	= 0.62388 	
xc	= 5772.8964	
w	= 0.78591 	
A	= 0.61563 	
set label 1 'Slit width $=\ 14\ \mathrm{\mu m}$, measurement 1' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit14-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit14-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit14-2.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit14-2.tex"
#####################
y0	= 0.6677  	
xc	= 5772.93913	
w	= 0.83995 	
A	= 0.78719 	
set label 1 'Slit width $=\ 14\ \mathrm{\mu m}$, measurement 2' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit14-2.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit14-2.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit16-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit16-1.tex"
#####################
y0	= 0.62558	   
xc	= 5772.88921	
w	= 0.7758  	
A	= 0.89765  	
set label 1 'Slit width $=\ 16\ \mathrm{\mu m}$, measurement 1' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit16-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit16-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit16-2.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit16-2.tex"
#####################
y0	= 0.67322 	
xc	= 5772.66851	
w	= 0.83266 	
A	= 0.96166 	
set label 1 'Slit width $=\ 16\ \mathrm{\mu m}$, measurement 2' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit16-2.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit16-2.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit18-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit18-1.tex"
#####################
y0	= 0.62147   
xc	= 5773.0391	
w	= 0.66301 
A	= 0.70982 
set label 1 'Slit width $=\ 18\ \mathrm{\mu m}$, measurement 1' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit18-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit18-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit18-2.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit18-2.tex"
#####################
y0	= 0.62965 	
xc	= 5772.86811	
w	= 0.83166 	
A	= 1.09143 	
set label 1 'Slit width $=\ 18\ \mathrm{\mu m}$, measurement 2' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit18-2.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit18-2.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   slit20-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit20.tex"
#####################
y0	= 0.63564  
xc	= 5772.85482	
w	= 0.80513 
A	= 1.38817 
set label 1 'Slit width $=\ 20\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit20-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit20-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit24-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit24.tex"
#####################
y0	= 0.62041 	
xc	= 5772.76352	
w	= 0.83296 	
A	= 0.66654 	
set label 1 'Slit width $=\ 24\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit24-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit24-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit28-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit28.tex"
#####################
y0	= 0.62188 
xc	= 5772.7006	
w	= 0.8794  
A	= 0.93297 
set label 1 'Slit width $=\ 28\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit28-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit28-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit32-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit32.tex"
#####################
y0	= 0.62976 	
xc	= 5771.80656
w	= 0.91541 	
A	= 1.26983 	
set label 1 'Slit width $=\ 32\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit32-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit32-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   slit36-1.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit36.tex"
#####################
y0	= 0.63171 	
xc	= 5772.41133		
w	= 0.9312  	
A	= 1.52062 	
set label 1 'Slit width $=\ 36\ \mathrm{\mu m}$' at graph 0,1.03
#####################
xl = 5765 	   	  
xh = 5781
yl = -0.3*y0
yh = 1.2*(A/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 1.5 lt 0
set arrow from xc,yl to xc,yh as 7
#####################
y(x) = y0 + (A/(w*sqrt(pi/2)))*exp(-2*((x-xc)/w)**2)
hline(x) = 0 
####################
plot 'slit36-1.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt 3 lw 1 t 'Gaussian Fit',\
	 'slit36-1.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   slit_linear.dat#
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "slit_linear.tex"
#####################
xl = 8	   	  
xh = 40
yl = 0.85
yh = 1.15
#####################
set xrange [xl:xh]
set yrange [yl:yh]
set sample 100000
set xtics 2
set mxtics 2
set ytics 0.05
set mytics 5
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Slit Width ($\mathrm{\mu m}$)'
set ylabel 'FWHM (\AA)' offset 2.5
unset arrow
unset label 1
#####################
lines1(x) = ks1*x+bs1
lines2(x) = 0*x+bs2
#####################
fit [23:40] lines1(x) 'slit_linear.dat' using 1:2 via ks1, bs1
fit [8:25] lines2(x) 'slit_linear.dat' using 1:2 via  bs2
#####################
plot 'slit_linear.dat' using 1:2:3 with yerrorbars pt 6 pointsize 1 lw 2 t 'Data' ,\
	 lines1(x) lt -1 lw 1 t 'Fit for Width $>23\ \mathrm{\mu m}$',\
	 lines2(x) lt 3 lw 4 t  'Fit for Width $<23\ \mathrm{\mu m}$'
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   ca_linear.dat  #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "ca_linear.tex"
#####################
xl = 3500	   	  
xh = 7000
yl = 3500
yh = 7000
#####################
set xrange [xl:xh]
set yrange [xl:xh]
set sample 100000
set xtics 500
set mxtics 10
set ytics 500
set mytics 10
set key box
set key noreverse
set key width -4
set key left at graph 0.03, 0.97
set xlabel 'Measured Wavelength (\AA)'
set ylabel 'Given Wavelength (\AA)' offset 2.5
unset arrow
unset label 1
#####################
linec(x) = kc*x+bc
#####################
fit linec(x) 'ca_linear.dat' using 1:3 via kc,bc
#####################
plot 'ca_linear.dat' using 1:3  pt 1 pointsize 1.7 lw 2 t 'Data' ,\
	 linec(x) lt 3 lw 1 t 'Linear Fit'
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   alpha-01.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "alpha01.tex"
#####################

y0	= 0.66762 	
xc1	= 6566.23053
A1	= 0.54714		
w1	= 0.75776		
xc2	= 6565.24272
A2	= 0.27225		
w2	= 2.13693		

set label 1 '$\alpha-$line, measure 1' at graph 0,1.03
#####################
xl = 6558	   	  
xh = 6573
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'alpha-01.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'alpha-01.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   alpha-02.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "alpha02.tex"
#####################

y0	= 0.66794 	
xc1	= 6566.17984	
A1	= 0.48732 	
w1	= 0.72065 	
xc2	= 6565.23262	
A2	= 0.27896 	
w2	= 2.05361 	

set label 1 '$\alpha-$line, measure 2' at graph 0,1.03
#####################
xl = 6558	   	  
xh = 6573
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'alpha-02.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'alpha-02.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################




####################
#                  #
#   beta-01.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "beta01.tex"
#####################

y0	= 0.60549 	
xc1	= 4864.31221
A1	= 0.70549 	
w1	= 0.8187  	
xc2	= 4863.06159
A2	= 0.09046 	
w2	= 0.90647 	

set label 1 '$\beta-$line, measure 1' at graph 0,1.03
#####################
xl = 4857	   	  
xh = 4869
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'beta-01.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'beta-01.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   beta-02.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "beta02.tex"
#####################

y0	= 0.60498 
xc1	= 4864.25865
A1	= 0.65619 
w1	= 0.78441 
xc2	= 4863.07484
A2	= 0.12839 
w2	= 1.16387 

set label 1 '$\beta-$line, measure 2' at graph 0,1.03
#####################
xl = 4857	   	  
xh = 4869
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'beta-02.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'beta-02.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   gamma-01.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "gamma01.tex"
#####################

y0	= 0.5802  	
xc1	= 4343.46617
A1	= 0.77853 	
w1	= 0.6492  	
xc2	= 4342.35343
A2	= 0.25439 	
w2	= 1.6357  	

set label 1 '$\gamma-$line, measure 1' at graph 0,1.03
#####################
xl = 4337	   	  
xh = 4348
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'gamma-01.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'gamma-01.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   gamma-02.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "gamma02.tex"
#####################

y0	= 0.5773	   
xc1	= 4343.48638	
A1	= 0.78172 	
w1	= 0.65155 	
xc2	= 4342.2538	
A2	= 0.24197 	
w2	= 1.54253 	

set label 1 '$\gamma-$line, measure 2' at graph 0,1.03
#####################
xl = 4337	   	  
xh = 4348
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'gamma-02.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'gamma-02.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   gamma-03.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "gamma03.tex"
#####################

y0	= 0.57826 	
xc1	= 4343.56664
A1	= 0.70684 	
w1	= 0.61019 	
xc2	= 4342.65345
A2	= 0.29495 	
w2	= 1.8448  	

set label 1 '$\gamma-$line, measure 3' at graph 0,1.03
#####################
xl = 4337	   	  
xh = 4348
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'gamma-03.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'gamma-03.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

####################
#                  #
#   delta-01.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "delta01.tex"
#####################

y0	= 0.63861 	
xc1	= 4104.69634	
A1	= 0.48551 	
w1	= 0.68703 	
xc2	= 4103.76306	
A2	= 0.0923  	
w2	= 0.97749 	

set label 1 '$\delta-$line, measure 1' at graph 0,1.03
#####################
xl = 4097	   	  
xh = 4109
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'delta-01.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'delta-01.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################


####################
#                  #
#   delta-02.dat   #
#                  #
####################
set terminal epslatex color standalone lw 1 header '\renewcommand{\normalsize}{\scriptsize} \newcommand{\dd}{\mathrm{d}} '
set output "delta02.tex"
#####################

y0	= 0.63861 	
xc1	= 4104.61262
A1	= 0.46645 	
w1	= 0.69812 	
xc2	= 4103.97838
A2	= 0.13635 	
w2	= 1.45973 	

set label 1 '$\delta-$line, measure 2' at graph 0,1.03
#####################
xl = 4097	   	  
xh = 4109
yl = -0.3*y0
yh = 1.2*(A1/(w*sqrt(pi/2))+y0)
#####################
set xrange [xl-0.1:xh+0.1]
set yrange [yl:yh]
set sample 100000
set xtics 1
set mxtics 5
set ytics 0.5
set mytics 5
set key box
set key noreverse
set key width -2
set key left at graph 0.03, 0.97
set xlabel 'Wavelength $(\mathrm{\AA})$'
set ylabel 'Intensity' offset 2.5
unset arrow
set style arrow 7 nohead lw 2 lt 0
set arrow from xc1,yl to xc1,yh as 7
set arrow from xc2,yl to xc2,yh as 7
#####################
y(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)+(A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
y1(x) = y0 + (A1/(w1*sqrt(pi/2)))*exp(-2*((x-xc1)/w1)**2)
y2(x) = y0 + (A2/(w2*sqrt(pi/2)))*exp(-2*((x-xc2)/w2)**2)
hline(x) = 0 
####################
plot 'delta-02.dat' u 2:1 pt 2 pointsize 1 lw 2 t 'Data' ,\
	 y(x) lt -1 lw 1 t 'Sum',\
	 y1(x) lt 3 lw 2 t 'Peak 1',\
	 y2(x) lt 3 lw 2 t 'Peak 2',\
	 'delta-02.dat' u ($2):($1-y($2)) pt 1 pointsize 0.7 lw 2 lc 3 t 'Residual',\
	 hline(x) lt -1 lw 0.5 t ''
#####################
set output
####################
#      END         #
####################

set terminal X11


