\contentsline {section}{\numberline {1}\uppercase {Introduction}}{3}{section.1}
\contentsline {section}{\numberline {2}\uppercase {Theory Preparation}}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Bohr Model}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Schr\"{o}dinger Equation for Hydrogen Atom}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Orbits and degeneracy}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Probability density of the electron position}{6}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Corrections}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Selection Rules}{9}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Line Width}{10}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Isotope Shift}{11}{subsection.2.6}
\contentsline {section}{\numberline {3}\uppercase {Experiment}}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Instrumentation}{12}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}The Grating Spectrometer}{12}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Caliberation Sources}{13}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Hydroge/Deuterium Sources}{13}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Light Detector}{14}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Software}{14}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Measurement equipment}{14}{subsubsection.3.1.6}
\contentsline {subsection}{\numberline {3.2}Procedure}{14}{subsection.3.2}
\contentsline {section}{\numberline {4}\uppercase {Result and Analysis}}{15}{section.4}
\contentsline {subsection}{\numberline {4.1}Optimization of Slit Width}{15}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Calibration}{16}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Balmer $\alpha -,\beta -,\gamma -,\delta -$ Lines of Hydrogen and Deuterium}{19}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Calculation of Rydberg Constants}{24}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Calculation of $m_e/m_{H}$}{25}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Mass of Deuterium Atom}{25}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Calculation of $e/m_e$}{26}{subsection.4.7}
\contentsline {section}{\numberline {5}\uppercase {Discussion}}{26}{section.5}
\contentsline {section}{\numberline {6}\uppercase {References}}{27}{section.6}
\contentsline {section}{\numberline {7}\uppercase {Exercises}}{27}{section.7}
\contentsline {section}{Appendices}{31}{section*.2}
\contentsline {section}{Appendix \numberline {A}{Slit Adjustment Spectra and Data}}{31}{Appendix.1.A}
\contentsline {section}{Appendix \numberline {B}{Calibration Spectra and Data}}{39}{Appendix.1.B}
